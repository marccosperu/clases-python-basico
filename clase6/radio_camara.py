# Crear el radio de alcance de la camara

# Importar la libreria folium
import folium

lat = -12.058308
lon = -77.024329

m = folium.Map(location=[lat, lon],zoom_start=17)

# Camara Av Nicolas de Pierola con Jr Huanta
folium.Marker(  location = [lat, lon],
                popup='%s %s'%(lat,lon),
                icon=folium.Icon(color='blue', icon='camera')).add_to(m)

# Radio de la camara
folium.CircleMarker(
    location=[lat,lon],
    radius=100,
    popup='Radio de la Camara',
    color='#3186cc',
    fill=True,
    fill_color='#3186cc'
).add_to(m)

m.save('radio_camaras.html')