import pandas as pd
import geopandas as gp
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point

import get_lat_lon
# Coordenadas 100 elegidas aleatoriamente en latitud y longitud (Robos)
coods_lon_lat = get_lat_lon.random_lon_lat(n_coord=200)

# Crear en objeto pandas
df = pd.DataFrame(np.array(coods_lon_lat), columns=['Longitud','Latitud'])


# create Geometry series with lat / longitude
geometry = [Point(xy) for xy in zip(df.Longitud, df.Latitud)]
df = df.drop(['Longitud', 'Latitud'], axis = 1)

# Create GeoDataFrame
points = gp.GeoDataFrame(df, geometry=geometry)

# Create Matplotlib figure
fig, ax = plt.subplots()

# Set Axes to equal (otherwise plot looks weird)
ax.set_aspect('equal')

# Plot GeoDataFrame on Axis ax
points.plot(ax=ax,marker='o', color='red', markersize=5)
#
#
# Create new point
center_coord = [geometry[100]]
center = gp.GeoDataFrame(geometry=center_coord)
#
# Plot new point
center.plot(ax=ax,color = 'blue',markersize=5)
#
#
# Buffer point and plot it
circle = gp.GeoDataFrame(geometry=center.buffer(0.0036))
#
circle.plot(color = 'white',ax= ax)
#
# # Calculate the points inside the circle
#
pointsinside = gp.sjoin(points,circle,how="inner")
#
#
 # Create a nice plot
fig, ax = plt.subplots()
ax.set_aspect('equal')
circle.plot(color = 'silver',ax=ax)
center.plot(ax=ax,color = 'blue',markersize=5)
pointsinside.plot(ax=ax,marker='o', color='green', markersize=20)
# print pointsinside.geometry

for point in pointsinside.geometry:
     print point.x , point.y
#
#
#
len(pointsinside.geometry)
plt.show()