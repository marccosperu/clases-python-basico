import numpy as np

def random_lat_lon(lat=-12, lon=-77,n_coord = 50):

    dec_lat = np.random.rand(n_coord)
    dec_lon = np.random.rand(n_coord)

    dec_lat_4d = np.round(dec_lat, decimals=4)
    dec_lat_1d = np.round(dec_lat, decimals=1)

    dec_lon_4d = np.round(dec_lon,decimals=4)
    dec_lon_1d = np.round(dec_lon, decimals=1)

    lat_lon = []

    for i in range(n_coord):
        dec_lat = max([dec_lat_1d[i], dec_lat_4d[i]]) - min([dec_lat_1d[i], dec_lat_4d[i]])
        dec_lon = max([dec_lon_1d[i], dec_lon_4d[i]]) - min([dec_lon_1d[i], dec_lon_4d[i]])

        if lat < 0 :
            lat_ = lat - dec_lat
        else:
            lat_ = lat + dec_lat
        if lon < 0 :
            lon_ = lon - dec_lon
        else:
            lon_ = lon + dec_lon

        lat_lon.append([lat_,lon_])

    return lat_lon



def random_lon_lat(lat=-12, lon=-77,n_coord = 50):

    dec_lat = np.random.rand(n_coord)
    dec_lon = np.random.rand(n_coord)

    dec_lat_4d = np.round(dec_lat, decimals=4)
    dec_lat_1d = np.round(dec_lat, decimals=1)

    dec_lon_4d = np.round(dec_lon,decimals=4)
    dec_lon_1d = np.round(dec_lon, decimals=1)

    lon_lat = []

    for i in range(n_coord):
        dec_lat = max([dec_lat_1d[i], dec_lat_4d[i]]) - min([dec_lat_1d[i], dec_lat_4d[i]])
        dec_lon = max([dec_lon_1d[i], dec_lon_4d[i]]) - min([dec_lon_1d[i], dec_lon_4d[i]])

        if lat < 0 :
            lat_ = lat - dec_lat
        else:
            lat_ = lat + dec_lat
        if lon < 0 :
            lon_ = lon - dec_lon
        else:
            lon_ = lon + dec_lon

        lon_lat.append([lon_,lat_])

    return lon_lat

# datos = np.random.randn(10)
# for dato in datos:
#     print dato
# print
# d1 = np.random.random_integers(12,12, size=(2,1))
# d2 = np.random.random_integers(77,77, size=(2,1))
#
# for i in d1:
#     print type(i[0])
#     print i
#
#
#
# numero = 50
# lat = np.random.rand(numero)
# lon = np.random.rand(numero)

# lat_lon = []
#
# for i in range(numero):
#     lat_r1 = float("{0:.1}".format(lat[i]))
#     lat_r2 = float("{0:.4}".format(lat[i]))
#     lat_f = max([lat_r1,lat_r2]) - min([lat_r1,lat_r2])
#
#     lon_r1 = float("{0:.1}".format(lon[i]))
#     lon_r2 = float("{0:.4}".format(lon[i]))
#     lon_f = max([lon_r1, lon_r2]) - min([lon_r1, lon_r2])
#     lat_lon.append([lat_f,lon_f])
#
#
#
#     lat=lat_f*-12
#     lon=lon_f* -12


#


