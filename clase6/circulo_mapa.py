#Crear un poligono 'circulo'

# Importar la libreria folium
import folium

# Crear mapa
m = folium.Map(location=[-12.1090135,-77.0294058],zoom_start=17)

# Crear punto
folium.Marker([-12.1090135,-77.0294058]).add_to(m)

#Crear poligono
folium.CircleMarker(
    location=[-12.1090135,-77.0294058],
    radius=300,
    popup='Python para no programadores',
    color='#3186cc',
    fill=True,
    fill_color='#3186cc'
).add_to(m)

# Guardar mapa
m.save('Miraflores_circulo.html')