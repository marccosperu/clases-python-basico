# Visualizar mapa del delito
import folium # Visualizar mapa
import numpy as np
import get_lat_lon # Obtener datos Longitud y Latitud
import random

# Coordenadas 100 elegidas aleatoriamente en latitud y longitud (Robos)
coods_lat_lon = get_lat_lon.random_(n_coord=200)

# Obtener la coordenada media
cood_media = np.array(coods_lat_lon).mean(axis=0)

# Crear mapa
m = folium.Map(location=[cood_media[0], cood_media[1]],zoom_start=15)

# Agregar puntos de robos en mapa
for lat, lon in coods_lat_lon:
    folium.Marker(  location = [lat, lon],
                            popup='%s %s'%(lat,lon),
                            icon=folium.Icon(color='red', icon='info-sign')).add_to(m)


# Agregar camaras y radio de las camaras
n_camaras = 10 # cantidad de camaras
radio = 50 #metros
d={}
for i in range(n_camaras):
    lat, lon = random.choice(coods_lat_lon)
    lat,lon = lat + 0.0010, lon + 0.0010

    if d.has_key('%s %s'%(lat,lon)) == False:

        folium.Marker(  location = [lat, lon],
                        popup=' Camara %s : %s %s'%(i+1,lat,lon),
                        icon=folium.Icon(color='blue', icon='camera')).add_to(m)
        d['%s %s'%(lat,lon)] = [lat,lon]

        # Radio de la camara
        folium.CircleMarker(
            location=[lat, lon],
            radius=radio,
            popup='Radio Camara %s ' % (i + 1),
            color='#3186cc',
            fill=True,
            fill_color='#3186cc'
        ).add_to(m)

# Guardar mapa
m.save('mapa_del_delito.html')