# Crear punto de la camara

# Importar la libreria folium
import folium

# Crear mapa
m = folium.Map(location=[-12.0528, -77.0595],zoom_start=17)

#Camara Av Nicolas de Pierola con Jr Huanta
lat = -12.058308
lon = -77.024329
folium.Marker(  location = [lat, lon],
                popup='%s %s'%(lat,lon),
                icon=folium.Icon(color='orange', icon='camera')).add_to(m)

# Guardar mapa
m.save('camaras.html')
