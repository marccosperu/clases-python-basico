# Crear un punto en el mapa

# Importar la libreria folium
import folium

#Crear mapa
m = folium.Map(location=[-12.1090135,-77.0294058],zoom_start=17)

#Crear punto
folium.Marker([-12.1090135,-77.0294058]).add_to(m)

# Guardar mapa
m.save('Miraflores_punto.html')