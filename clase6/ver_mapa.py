#Folium libreria para visualizar datos en mapas

# Importar la libreria folium
import folium

# Crear mapa
m = folium.Map(location=[-12.1090135,-77.0294058],zoom_start=5)

# Guardar mapa
m.save('mapa_peru.html')