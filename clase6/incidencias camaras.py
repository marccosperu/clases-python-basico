# Crear puntos de las incidencias y las camaras en el mapa desde un csv

# Importar la libreria folium
import folium
import random

# Crear mapa
m = folium.Map(location=[-12.0528, -77.0595],zoom_start=15)

# Lectura de puntos
nombre_arch_csv = 'puntos_incidentes.csv'
arch_csv = open(nombre_arch_csv,'r')
lineas = arch_csv.readlines()
arch_csv.close()

lista_lat_lon = []
# Agregar al mapa los puntos de incidentes reportados
for linea in lineas[1:]:
    linea_l = linea.split(',')
    lat = float(linea_l[0])
    lon = float(linea_l[1])

    folium.Marker(  location = [lat, lon],
                        popup='%s %s'%(lat,lon),
                        icon=folium.Icon(color='red', icon='info-sign')).add_to(m)

    lista_lat_lon.append([lat,lon])


lat,lon = random.choice(lista_lat_lon)

lat,lon = lat + 0.0010, lon + 0.0010


folium.Marker(  location = [lat, lon],
                popup='%s %s'%(lat,lon),
                icon=folium.Icon(color='blue', icon='camera')).add_to(m)


# Guardar mapa
m.save('incidencias_camara.html')