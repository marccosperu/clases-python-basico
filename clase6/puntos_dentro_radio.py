import pandas as pd
import geopandas as gp
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point

# Create Fake Data
df = pd.DataFrame(np.random.randint(10, 20,size=(35, 3)), columns=['Longitude','Latitude','data'])
print df

print df.Longitude

print df.Latitude

# create Geometry series with lat / longitude
geometry = [Point(xy) for xy in zip(df.Longitude, df.Latitude)]
df = df.drop(['Longitude', 'Latitude'], axis = 1)

# Create GeoDataFrame
points = gp.GeoDataFrame(df, crs=None, geometry=geometry)

# Create Matplotlib figure
fig, ax = plt.subplots()

# Set Axes to equal (otherwise plot looks weird)
ax.set_aspect('equal')

# Plot GeoDataFrame on Axis ax
points.plot(ax=ax,marker='o', color='red', markersize=5)

# Create new point
center_coord = [Point(15, 13)]
center = gp.GeoDataFrame(crs=None, geometry=center_coord)

# Plot new point
center.plot(ax=ax,color = 'blue',markersize=5)

# Buffer point and plot it
circle = gp.GeoDataFrame(crs=None, geometry=center.buffer(2.5))

circle.plot(color = 'white',ax= ax)

# Calculate the points inside the circle

pointsinside = gp.sjoin(points,circle,how="inner")


# Create a nice plot
#fig, ax = plt.subplots()
ax.set_aspect('equal')
circle.plot(color = 'silver',ax=ax)
center.plot(ax=ax,color = 'blue',markersize=5)
pointsinside.plot(ax=ax,marker='o', color='green', markersize=5)
print pointsinside.geometry
for point in pointsinside.geometry:
    print point.x , point.y



plt.show()