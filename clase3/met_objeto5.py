# Metodos de objetos : Archivo ( open : write , close)

archivo = open('mi_archivo_csv.csv','w')
archivo.write('Nombre, Apellido, DNI \n')
archivo.write('Jaime, Castillo, 98988979 \n')
archivo.write('Willi, Huaman, 98798787 \n')
archivo.write('Miguel, Palacios, 98989898 \n')
archivo.write('Heidy,Melendrez,987987987 \n')
archivo.write('Valeria,Ledesma,987987987 \n')
archivo.close()
print 'Archivo csv creado'
