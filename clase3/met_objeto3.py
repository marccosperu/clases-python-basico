#Metodos de Objetos : Diccionario (hast_key, keys y values)
#Mama busca a su hijo 
#Consultar las claves del diccionario

print 'Fiesta Halloween!!'
print 
print 'Mama busca a su hijo en la fiesta'
print 
invitados = []
invitados.append('Marco')
invitados.append('Kevin')
invitados.append('Diego')
invitados.append('Miguel')
invitados.append('Willy')

dic = {'registro':invitados,'cantidad':len(invitados),'lugar':'El 5to No paga'}

consulta = raw_input('Consulta : ')

if dic.has_key(consulta) == True:
	print "Si existe tu consulta '%s' "%consulta
	print "Valor de la consulta '%s' es : %s"%(consulta,dic[consulta])
	
else:
	print "No existe tu consulta '%s'"%consulta
	
print 
print 'Mostrar las claves'
print dic.keys()
print 
print 'Mostrar los valores'
print dic.values()
print
print 'Mama encontro a su hijo y lo borra del registro'
dic['registro'].remove('Marco')
print 
print dic['registro']
print 'Marco se quedo sin fiesta  :(' 
