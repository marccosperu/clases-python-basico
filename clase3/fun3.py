#Funciones : Variables locales
# 'res' variable local creado dentro de una funcion

def promedio(lista):
	res = sum(lista)/len(lista)
	return res
	
	
edades = [24,25,30,28,25,22,21]
print 'edades : %s '%edades
print 
p_edad = promedio(edades)
print 'El promedio de edades es %s '%p_edad
print
max_edad = max(edades)
min_edad = min(edades)
print 'La mayor edad es %s '%max_edad
print
print 'La menor edad es %s '%min_edad
