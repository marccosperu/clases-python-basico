#Modulos : Modulos del leguaje

#importando modulo sistema operativo 'import os'
import os
#tiempo
import time
#Aleatorio
import random

#Muestra la ruta donde estamos
print os.getcwd()
#Es un archivo que existe en el directorio
print os.path.isfile('modulos_del_lenguaje.py')
#Muestra la lista de archivos y/o directorios
print os.listdir(os.getcwd())

#Muestra el timepo computacional
print time.time()
#Duerme 10 segundos
time.sleep(1)

#Selecciona al azar un archivo del directorio actual
archivo = random.choice(os.listdir('.'))
#Junta la ruta con el archivo
ruta_archivo = os.path.join(os.getcwd(),archivo)
print 'python %s'%archivo
#ejecuta el archivo seleccinado
os.system("""python "%s" """%ruta_archivo)
