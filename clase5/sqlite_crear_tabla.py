try:
    import MySQLdb as db
except:
    try:
        import cx_Oracle as db
        con='kiosko/kiosko@localhost'
    except:
        import sqlite3 as db
        con='basedat.db'
    mysql=False
else:
    mysql=True


if mysql:
    conexion=db.connect(db='ejemplobd',host='localhost',user='root',passwd='')
else:
    conexion=db.connect(con)

cursor=conexion.cursor()

cursor.execute('''create table empleados (dni text,nombre text)''')
cursor.execute('''insert into empleados values ('09598414','Alfonso de la Guarda')''')
cursor.execute('''insert into empleados values ('40621298','Marco Ramos')''')
conexion.commit()
