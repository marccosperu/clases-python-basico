import pandas as pd
import matplotlib.pyplot as plt
import os
 
df = pd.read_csv("notas.csv")
 
tab = pd.crosstab(index=df["nota"],columns="frecuencia")
 
plt.bar(tab.index,tab["frecuencia"])
plt.xlabel("Notas del examen")
plt.savefig("notas2.png")
#os.startfile("notas2.png")
