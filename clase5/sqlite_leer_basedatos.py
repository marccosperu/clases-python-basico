import sqlite3 as db

conexion = db.connect("basedat.db")
cursor = conexion.cursor()

#Consulta en forma de sentencia sql
sql='select * from programadores'
cursor.execute(sql)

#Obtiene todas las filas (restantes) de un resultado de consulta
filas_tabla = cursor.fetchall()

#Recorrer la lista de filas y mostrarlas
for fila in filas_tabla:
    print fila

conexion.close()

print filas_tabla
