#Crear un archivo de hoja de calculo(excel)
#Agregar una tabla 

#Importar Workbook desde xlwt
from xlwt import  Workbook
#Modulo 'box' para crear tabla
import box
import os
import time

#Crear una variable objeto libro/archivo
w = Workbook()
#Crear una hoja del libro
ws1 = w.add_sheet('Reporte')
#Datos de la tabla a escribir 
datos = [
		['Nombre','Edad','DNI'],
		['Marco',36,40621298],
		['Luis',30,40344844],
		['Ana',25,47097097],
		['Ariel',18,44987876],
		['Antonio',25,58568745],
		['Julio',37,40857555]
							]
#Escribir por fila la tabla
box.rowtable(ws1,1,1,datos)
tiempo = int(time.time())
#Nombre del archivo en funcion al tiempo
nombre_arch = 'archivo_creado_%s.xls'%(tiempo)
#guardar el libro/archivo 
w.save(nombre_arch)
#Abrir el archivo 
#os.startfile(nombre_arch)
