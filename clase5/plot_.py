from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def plot_pnts(x_,y_,z_):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')

    #ax.scatter(x_, y_, z_, c='mediumaquamarine',marker = 'o')
    ax.scatter(x_, y_, z_, c='red',marker = 'o',s = 50)#configurar
    ax.text2D(0.05, 0.95, "%s Puntos"%(len(x_)), transform=ax.transAxes)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    ax.set_xlim3d(min(x_), max(x_))
    ax.set_ylim3d(min(y_), max(y_))
    ax.set_zlim3d(min(z_), max(z_))
    
    plt.show()
    
def plot_pnts_collares_malla(x_c,y_c,z_c,x_m,y_m,z_m):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')

    ax.scatter(x_c, y_c, z_c, c='mediumaquamarine',marker = 'o')
    ax.scatter(x_m, y_m, z_m, c='whitesmoke',marker = '^')#s=size
    #ax.legend()
    ax.text2D(0.05, 0.95, "%s Puntos (o) de Collares - %s Puntos de la Malla (^)"%(len(x_c),len(x_m)), transform=ax.transAxes)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    #ax.set_xlim3d(min(x_c), max(x_c))
    #ax.set_ylim3d(min(y_c), max(y_c))
    #ax.set_zlim3d(min(z_c), max(z_c))
    dx = max(x_c) - min(x_c)
    dy = max(y_c) - min(y_c)
    dz = max(z_c) - min(z_c)
    
    
    if dx > dy :
        d = dx - dy
        ax.set_xlim3d(min(x_c), max(x_c))
        ax.set_ylim3d(min(y_c), max(y_c)+d)
    elif dx < dy:
        d = dy - dx
        ax.set_ylim3d(min(x_c), max(x_c)+d)
        ax.set_ylim3d(min(y_c), max(y_c))
    else:
        d=0
        ax.set_ylim3d(min(x_c), max(x_c))
        ax.set_ylim3d(min(y_c), max(y_c))
    ax.set_zlim3d(min(z_c), max(z_c))
    
    plt.show()

def plot_pnts_seleccion(x,y,z,x_m,y_m,z_m,x_s,y_s,z_s,x_p,y_p,z_p,x_sp,y_sp,z_sp):
    fig2 = plt.figure(2)
    ax = fig2.add_subplot(111,projection='3d')
    ax.plot(x,y,z,'o',c='mediumaquamarine',label='Collar(%s p)'%len(x))
    ax.plot(x_m,y_m,z_m,'^',c='whitesmoke',label='Malla(%s p)'%len(x_m))
    ax.plot(x_s,y_s,z_s,'o',c='b',label='Malla Nueva(%s p)'%len(x_s))
    #ax.scatter(x, y, z, c='r',marker = 'o')
    #ax.scatter(x_m, y_m, z_m, c='b',marker = '^')
    #ax.scatter(x_s, y_s, z_s, c='g',marker = 's',s=40)
    ax.text2D(0.05, 0.95, "%s Puntos de la Malla Nueva"%(len(x_s)), transform=ax.transAxes)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    dx = max(x) - min(x)
    dy = max(y) - min(y)
    
    if dx > dy :
        d = dx - dy
        ax.set_ylim3d(min(x), max(x))
        ax.set_ylim3d(min(y), max(y)+d)
    elif dx < dy:
        d = dy - dx
        ax.set_ylim3d(min(x), max(x)+d)
        ax.set_ylim3d(min(y), max(y))
    else:
        d=0
        ax.set_ylim3d(min(x), max(x))
        ax.set_ylim3d(min(y), max(y))
    ax.set_zlim3d(min(z), max(z))
    
    ax.legend(loc='upper left', numpoints=1, ncol=3, bbox_to_anchor=(0, 0))
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111,projection='3d')
    ax.plot(x_p,y_p,z_p,'o',c='mediumaquamarine',label='Collar Proyectado(%s p)'%len(x))
    ax.plot(x_m,y_m,z_m,'^',c='whitesmoke',label='Malla(%s p)'%len(x_m))
    ax.plot(x_sp,y_sp,z_sp,'o',c='b',label='Malla Nueva Proyectado(%s p)'%len(x_s))
    #ax.scatter(x, y, z, c='r',marker = 'o')
    #ax.scatter(x_m, y_m, z_m, c='b',marker = '^')
    #ax.scatter(x_s, y_s, z_s, c='g',marker = 's',s=40)
    ax.text2D(0.05, 0.95, "%s Puntos de la Malla Nueva"%(len(x_s)), transform=ax.transAxes)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    dx = max(x) - min(x)
    dy = max(y) - min(y)
    
    if dx > dy :
        d = dx - dy
        ax.set_ylim3d(min(x), max(x))
        ax.set_ylim3d(min(y), max(y)+d)
    elif dx < dy:
        d = dy - dx
        ax.set_ylim3d(min(x), max(x)+d)
        ax.set_ylim3d(min(y), max(y))
    else:
        d=0
        ax.set_ylim3d(min(x), max(x))
        ax.set_ylim3d(min(y), max(y))
    ax.set_zlim3d(min(z), max(z))
    
    ax.legend(loc='upper left', numpoints=1, ncol=3, bbox_to_anchor=(0, 0))

    plt.show()
    
def plot_pnts_seleccion2(cx_p,cy_p,cz_p,x,y,z,x_m,y_m,z_m,x_s,y_s,z_s):
    fig = plt.figure(1)
    ax = fig.add_subplot(111,projection='3d')
    ax.plot(x,y,z,'o',c='w',label='Collar(%s p)'%len(x))
    ax.plot(x_m,y_m,z_m,'^',c=0,label='Malla Base(%s p)'%len(x_m))
    ax.plot(x_s,y_s,z_s,'o',c='b',label='Seleccionado(%s p)'%len(x_s))
    ax.plot(cx_p,cy_p,cz_p,'^',c='r',label='Collares proyectados (%s p)'%len(cx_p))
    
    
    #ax.scatter(x, y, z, c='r',marker = 'o')
    #ax.scatter(x_m, y_m, z_m, c='b',marker = '^')
    #ax.scatter(x_s, y_s, z_s, c='g',marker = 's',s=40)
    ax.text2D(0.05, 0.95, "%s Puntos Seleccionados"%(len(x_s)), transform=ax.transAxes)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    ax.set_xlim3d(min(x), max(x))
    ax.set_ylim3d(min(y), max(y))
    ax.set_zlim3d(min(z), max(z))
    ax.legend(loc='upper left', numpoints=1, ncol=3, bbox_to_anchor=(0, 0))
    
    fig = plt.figure(2)
    
    
    
    
    plt.show()
