#crear un archivo de hoja de calculo (excel)

#Importar Workbook desde xlwt
from xlwt import  Workbook

#Crear una variable objeto libro/archivo
w = Workbook()

#Crear una hoja del libro
ws1 = w.add_sheet('Reporte')

#escribir 'Marco' en la celda fila 0 columna 0 
ws1.write(0,0,'Marco')

#escribir '25' en la celda fila 0 columna 1 
ws1.write(0,1,25)

#guardar el libro/archivo 
w.save('archivo_creado.xls')
