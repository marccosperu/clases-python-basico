#Crear una base de datos

#Importar sqlite como db(data base)
import sqlite3 as db

#Nombre de mi base de datos
con='basedat.db'

#Crear/conectar a la base de datos
conexion=db.connect(con)
#Creando el objeto cursor
cursor=conexion.cursor()

# excute es un metodo para ejecutar comandos SQL
#Crear tabla 'programadores'
try :
	cursor.execute('''create table programadores (dni text,nombre text)''')
	#Insertar fila de datos
	cursor.execute('''insert into programadores values ('09598414','Alfonso de la Guarda')''')
	cursor.execute('''insert into programadores values ('40621298','Marco Ramos')''')
	cursor.execute('''insert into programadores values ('40621297','Diego Ramirez')''')
	cursor.execute('''insert into programadores values ('40621296','Jaime Castillo')''')
	cursor.execute('''insert into programadores values ('40621295','Willy Huaman')''')
	cursor.execute('''insert into programadores values ('40621294','Miguel Palacios')''')
	cursor.execute('''insert into programadores values ('40621293','Heidy Melendrez')''')
	cursor.execute('''insert into programadores values ('40621292','Valeria Ledesma')''')
	#Guardar cambios
	conexion.commit()
	#Cerrar conexion
	conexion.close()

except Exception,error:
	print 'La tabla ya esta creada :',error



