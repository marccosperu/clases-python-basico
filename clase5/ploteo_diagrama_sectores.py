import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
 
df = pd.read_csv("notas.csv")
 
tab = pd.crosstab(index=df["nota"],columns="frecuencia")
 
aprobados = tab.loc[tab.index >= 5]["frecuencia"].sum()
desaprobados = tab.loc[tab.index < 5]["frecuencia"].sum()
 
data = np.array([aprobados,desaprobados])
plt.pie(data,labels=["Aprobados","Desaprobados"],autopct="%1.1f%%")
plt.xlabel("Notas del examen",fontsize=20)
plt.savefig("notas1.png")
#os.startfile("notas1.png")
