#c:\\python25

from xlwt import *

#Estilo de la celda
borders = Borders()
borders.left = 1
borders.right = 1
borders.top = 1
borders.bottom = 1
estilo_bordes_ = XFStyle()
estilo_bordes_.borders = borders

#Esta funcion crea bordes de tablas en columnas 
def coltable(sheet,row,col,data_lists):
  
    #Todas las lista a escribir en columnas
    for e in xrange(len(data_lists)):
        #Escribiendo cada elemento de la lista a una celda
        for i in xrange(len(data_lists[e])):
            
            data_list=data_lists[e]
            sheet.write(row +i,col+e,data_list[i],estilo_bordes_)

                  
#Esta funcion crea bordes de tablas en filas
def rowtable(sheet,row,col,data_lists):

    #Todas las lista a escribir en filas
    for e in xrange(len(data_lists)):
        #Escribiendo cada elemento de la lista a una celda  
        for i in xrange(len(data_lists[e])):
   
            data_list=data_lists[e]
            sheet.write(row +e,col+i,data_list[i],estilo_bordes_)
    

#Esta funcion crea bordes de tablas en columnas y en filas
def table(sheet,row_init,col_init,data_lists,formation):
  
    #Todas las lista a escribir 
    for e in xrange(len(data_lists)):
        #Escribiendo cada elemento de la lista a una celda
        for i in xrange(len(data_lists[e])):
            
            data_list=data_lists[e]
            if formation == 'col':
                sheet.write(row_init +i,col_init+e,data_list[i],estilo_bordes_)
            elif formation == 'row':
                sheet.write(row_init +e,col_init+i,data_list[i],estilo_bordes_)
                
##            
##w=Workbook()
##ws1=w.add_sheet('Valor de Atamiento')
##inirow=3
##inicol=3
##headers,bold=['Veta','Ag (g/t)','Au (g/t)','Cu (g/t)'],'bold'
##vetas=['Veta %s'%i for i in xrange(10)]
##Ags=['Ag %s'%i for i in xrange(10)]
##Aus=['Au %s'%i for i in xrange(10)]
##Cus=['Cu %s'%i for i in xrange(10)]
##
##data_lists=[ vetas,Ags,Aus,Cus ]
##
##table(ws1,inirow,inicol,data_lists)
##
##w.save('borders_.xls')

            
