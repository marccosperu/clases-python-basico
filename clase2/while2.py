#Control de flujos ( while )

#Accion repetitiva : Intentar adivinar el numero magico
#Condicion salir del while : Adivinar el numero magico

#Mostrar pistas para adivinar

numero_ganador = 68
while True:
	numero_ingresado = input("Ingresa el numero : ")
	
	if numero_ingresado < numero_ganador :
		print "Numero ganador es mayor"

	elif numero_ingresado > numero_ganador :
		print "Numero ganador es menor"
		
	else:
		print "ADIVINASTE EL NUMERO GANADOR!!!"
		break


