#Control de flujos ( while )

#Accion repetitiva : Incremeto de la edad
#Condicion salir del while : edad < 18

#Mostrar siempre que la variable 'edad' es menor a 18

edad = 0
while edad < 18:
	print 'Edad : %s ,eres menor de edad.'%edad
	edad = edad + 1
