#Control de flujos ( for )

#for aplicando a cadenas
#for aplindo a listas
#for aplicando a diccionarios


print 'for - cadenas'
letras = 'letras'

for letra in letras:
	print letra

print 
print 'for - lista'

lista_numeros = range(1,11)

for numero in lista_numeros:
	print numero

print
print 'for en diccionario'
dic = {'nombre':'alex','DNI':'47745332','cel':'987665432'}

for clave in dic:
	print clave,'=', dic [clave]


#######################################################################
print 
print 'for - letra de un diccionario'
for letra in dic['nombre']:
	print letra
print
	
print 'for - con comando len'
lista_vocales = ['a','b','c','d','e']
for orden in range(len(lista_vocales)):
	print lista_vocales[orden]
